import tensorflow as tf
import time, os
from tensorflow.contrib import layers
from utils.utils import get_batch_index
import numpy as np
import tflearn

class MLAANet_sharedE_sharedG_Model(object):
    def __init__(self, config, sess, data):
        self.embedding_dim = config.embedding_dim
        self.batch_size = config.batch_size
        self.n_epoch = config.n_epoch
        self.n_hidden = config.n_hidden
        self.n_class = config.n_class
        self.learning_rate = config.learning_rate
        self.l2_reg = config.l2_reg
        self.dropout = config.dropout
        self.use_batch_norm = config.use_batch_norm

        self.word2id = data["word2id"]
        self.max_aspect_len = data["max_aspect_len"]
        self.max_context_len = data["max_context_len"]
        self.word2vec = data["word2vec"]
        self.doc2vec = data["doc2vec"]
        self.lex_dim = data["lex_dim"]
        self.max_doc_len = data["max_doc_len"]
        self.data_size = data["data_size"]

        self.sess = sess
        self.seed = config.seed
        self.domain = config.domain
        self.gamma = config.gamma
        self.checkpoint_path = config.checkpoint_path

    def _attention_layer(self, inputs, length, name):
        with tf.variable_scope(name):
            u_context = tf.Variable(tf.truncated_normal([length]), name='u_context')
            h = layers.fully_connected(inputs, length, activation_fn=tf.nn.tanh)
            alpha = tf.nn.softmax(tf.reduce_sum(tf.multiply(h, u_context), axis=2, keep_dims=True), dim=1)
            att_output = tf.reduce_sum(tf.multiply(inputs, alpha), axis=1)
            return att_output

    def _length(self, sequences):
        used = tf.sign(tf.reduce_max(tf.abs(sequences), reduction_indices=2))
        seq_len = tf.reduce_sum(used, reduction_indices=1)
        return tf.cast(seq_len, tf.int32)

    def _interactive_attention(self, batch_size, weights, biases, max_len, aspect, context):
        context_outputs_iter = tf.TensorArray(tf.float32, 1, dynamic_size=True, infer_shape=False)
        context_outputs_iter = context_outputs_iter.unstack(context)
        aspect_iter = tf.TensorArray(tf.float32, 1, dynamic_size=True, infer_shape=False)
        aspect_iter = aspect_iter.unstack(aspect)

        context_avg_rep = tf.TensorArray(size=batch_size, dtype=tf.float32)
        context_avg = tf.TensorArray(size=batch_size, dtype=tf.float32)

        def _condition(i, context_avg_rep, context_avg):
            return i < batch_size

        def _body(i, context_avg_rep, context_avg):
            a = context_outputs_iter.read(i)
            b = aspect_iter.read(i)
            context_score = tf.reshape(tf.nn.tanh(tf.matmul(tf.matmul(a, weights), tf.reshape(b, [-1, 1])) + biases),[1, -1])

            context_avg_temp = tf.nn.softmax(context_score)
            context_avg = context_avg.write(i, context_avg_temp)
            context_avg_rep = context_avg_rep.write(i, tf.matmul(context_avg_temp, a))
            return (i + 1, context_avg_rep, context_avg)

        _, context_avg_rep_final, context_avg_final = tf.while_loop(cond=_condition, body=_body,
                                                                    loop_vars=(0, context_avg_rep, context_avg))

        context_avgs = tf.reshape(context_avg_final.stack(), [-1, max_len])
        context_avg_reps = tf.reshape(context_avg_rep_final.stack(), [-1, self.n_hidden])

        return context_avgs, context_avg_reps

    def _shared_layer(self, batch_size, weight, context):

        context_avg_rep = tf.TensorArray(size=batch_size, dtype=tf.float32)

        def _condition(i, context_avg_rep):
            return i < batch_size

        def _body(i, context_avg_rep):
            # a = context_outputs_iter.read(i)
            context_score = tf.reshape(tf.nn.relu(tf.matmul(context[i], tf.reshape(weight, [-1, context.shape[2]]))), [-1, context.shape[2]])

            context_avg_rep = context_avg_rep.write(i, context_score)

            return (i + 1, context_avg_rep)

        _, context_avg_rep_final = tf.while_loop(cond=_condition, body=_body, loop_vars=(0, context_avg_rep))

        context_avg_reps = context_avg_rep_final.stack()

        return context_avg_reps

    def _gated_interation_layer(self, batch_size, Weight, bias, a, b):

        context_avg_rep = tf.TensorArray(size=batch_size, dtype=tf.float32)

        def _condition(i, context_avg_rep):
            return i < batch_size

        def _body(i, context_avg_rep):
            # a = context_outputs_iter.read(i)
            context_score = tf.reshape(tf.multiply(tf.reshape(a[i], [1, -1]), tf.nn.relu(tf.matmul(tf.reshape(b[i], [1, -1]),
                                                                                                   tf.reshape(Weight, [-1, b.shape[1]])) + tf.transpose(bias))), [-1, b.shape[1]])

            context_avg_rep = context_avg_rep.write(i, context_score)

            return (i + 1, context_avg_rep)

        _, context_avg_rep_final = tf.while_loop(cond=_condition, body=_body, loop_vars=(0, context_avg_rep))

        context_avg_reps = tf.reshape(context_avg_rep_final.stack(), [-1, self.n_hidden])

        return context_avg_reps

    def evaluate_f_score(self, predicted, actual):
        import sklearn as sk

        precision = sk.metrics.precision_score(actual, predicted, average="macro")
        recall = sk.metrics.recall_score(actual, predicted, average="macro")
        f1 = sk.metrics.f1_score(actual, predicted, average="macro")

        return precision, recall, f1

    def build_model(self):
        if self.seed is not None:
            np.random.seed(self.seed)

        with tf.name_scope('inputs'):
            self.aspects = tf.placeholder(tf.int32, [None, self.max_aspect_len])
            self.contexts = tf.placeholder(tf.int32, [None, self.max_context_len])
            self.context_lex_embedding = tf.placeholder(tf.float32, [None, self.max_context_len, self.lex_dim])
            self.aspect_lex_embedding = tf.placeholder(tf.float32, [None, self.max_aspect_len, self.lex_dim])
            self.doc_lex_embedding = tf.placeholder(tf.float32, [None, self.max_doc_len, self.lex_dim])
            self.labels = tf.placeholder(tf.int32, [None, self.n_class])
            self.aspect_lens = tf.placeholder(tf.int32, None)
            self.context_lens = tf.placeholder(tf.int32, None)
            self.dropout_keep_prob = tf.placeholder(tf.float32)

            self.docs = tf.placeholder(tf.int32, [None, self.max_doc_len])
            self.doc_labels = tf.placeholder(tf.int32, [None, self.n_class])

            aspect_inputs = tf.nn.embedding_lookup(self.word2vec, self.aspects)
            aspect_inputs = tf.cast(aspect_inputs, tf.float32)
            aspect_inputs = tf.nn.dropout(aspect_inputs, keep_prob=self.dropout_keep_prob)

            context_inputs = tf.nn.embedding_lookup(self.word2vec, self.contexts)
            context_inputs = tf.cast(context_inputs, tf.float32)
            context_inputs = tf.nn.dropout(context_inputs, keep_prob=self.dropout_keep_prob)

            doc_inputs = tf.nn.embedding_lookup(self.doc2vec, self.docs)
            doc_inputs = tf.cast(doc_inputs, tf.float32)
            doc_inputs = tf.nn.dropout(doc_inputs, keep_prob=self.dropout_keep_prob)

        with tf.name_scope('weights'):
            weights = {
                'aspect_score': tf.get_variable(
                    name='W_a',
                    shape=[self.n_hidden, self.n_hidden],
                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'context_score': tf.get_variable(
                    name='W_c',
                    shape=[self.n_hidden, self.n_hidden],
                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'corellation_score': tf.get_variable(
                    name='W_corell',
                    shape=[self.max_doc_len, self.max_doc_len],
                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'softmax': tf.get_variable(
                    name='W_l',
                    shape=[self.n_hidden, self.n_class],
                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'doc_softmax': tf.get_variable(
                    name='W_d',
                    shape=[self.n_hidden, self.n_class],
                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
            }

        with tf.name_scope('biases'):
            biases = {
                'aspect_score': tf.get_variable(
                    name='B_a',
                    shape=[self.max_aspect_len, 1],
                    initializer=tf.zeros_initializer(),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'context_score': tf.get_variable(
                    name='B_c',
                    shape=[self.max_context_len, 1],
                    initializer=tf.zeros_initializer(),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'corellation_score': tf.get_variable(
                    name='B_corell',
                    shape=[self.max_doc_len, 1],
                    initializer=tf.zeros_initializer(),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
                'softmax': tf.get_variable(
                    name='B_l',
                    shape=[self.n_class],
                    initializer=tf.zeros_initializer(),
                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                ),
            }

        with tf.name_scope('input_tensor'):
            aspect_avg_inputs = tf.reduce_mean(aspect_inputs, 1)

            til_doc_hid = tf.tile(aspect_avg_inputs, [1, self.max_doc_len])
            til_doc_hid3dim = tf.reshape(til_doc_hid, [-1, self.max_doc_len, self.embedding_dim])
            doc_til_concat = tf.concat(axis=2, values=[doc_inputs, til_doc_hid3dim])
            doc_til_concat_lex = tf.concat(axis=2, values=[doc_til_concat, self.doc_lex_embedding])

            # Concat aspect vector with context embeddings
            til_hid = tf.tile(aspect_avg_inputs, [1, self.max_context_len])
            til_hid3dim = tf.reshape(til_hid, [-1, self.max_context_len, self.embedding_dim])
            # Concat aspect vectors with context embedding
            a_til_concat = tf.concat(axis=2, values=[context_inputs, til_hid3dim])
            # Concat context lexicon embedding with context embedding
            a_til_concat_lex = tf.concat(axis=2, values=[a_til_concat, self.context_lex_embedding])

            aspect_lex_concat = tf.concat(axis=2, values=[aspect_inputs, self.aspect_lex_embedding])
            aspect_lex_concat = tf.nn.dropout(aspect_lex_concat, keep_prob=self.dropout_keep_prob)

            batch_size = tf.shape(a_til_concat_lex)[0]
            shared_variable = tf.get_variable(name='W_shared',
                                              shape=[a_til_concat_lex.shape[2], a_til_concat_lex.shape[2]],
                                              initializer=tf.random_uniform_initializer(-0.01, 0.01),
                                              regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg),
                                              trainable=True
                                              ),
            a_til_concat_lex = self._shared_layer(batch_size, weight=shared_variable, context=a_til_concat_lex)
            doc_til_concat_lex = self._shared_layer(batch_size, weight=shared_variable, context=doc_til_concat_lex)

        with tf.variable_scope('doc'):

            doc_outputs, doc_state = tf.nn.dynamic_rnn(tf.contrib.rnn.LSTMCell(self.n_hidden),
                                                       inputs=doc_til_concat_lex,
                                                       sequence_length=self._length(doc_til_concat_lex),
                                                       dtype=tf.float32,
                                                       scope="doc_lstm")
            if self.use_batch_norm:
                doc_outputs = tflearn.layers.normalization.batch_normalization(doc_outputs)
                doc_outputs = tf.nn.relu(doc_outputs)

            doc_att = self._attention_layer(doc_outputs, length=self.n_hidden, name='doc_att')

        with tf.variable_scope('word_aspect_fusion'):

            aspect_outputs, aspect_state = tf.nn.dynamic_rnn(
                tf.contrib.rnn.LSTMCell(self.n_hidden),
                inputs=aspect_lex_concat,
                sequence_length=self._length(aspect_lex_concat),
                dtype=tf.float32,
                scope="aspect_lstm"
            )
            if self.use_batch_norm:
                aspect_outputs = tflearn.layers.normalization.batch_normalization(aspect_outputs)
                aspect_outputs = tf.nn.relu(aspect_outputs)

            batch_size = tf.shape(aspect_outputs)[0]

            context_outputs, context_state = tf.nn.dynamic_rnn(
                tf.contrib.rnn.LSTMCell(self.n_hidden),
                inputs=a_til_concat_lex,
                sequence_length=self._length(a_til_concat_lex),
                dtype=tf.float32,
                scope="context_lstm"
            )
            if self.use_batch_norm:
                context_outputs = tflearn.layers.normalization.batch_normalization(context_outputs)
                context_outputs = tf.nn.relu(context_outputs)

            #####################################
            # Interactive attention of Aspect average hidden vector and Context hidden vectors
            aspect_avg = tf.reduce_mean(aspect_outputs, 1)
            _, self.context_avg_reps = self._interactive_attention(batch_size=batch_size, weights=weights['context_score'],
                                                                   biases=biases['context_score'],
                                                                   max_len=self.max_context_len,
                                                                   aspect=aspect_avg, context=context_outputs)
            ########################################

            ########################################
            # Attention hidden vector and Context hidden vectors
            aspect_att = self._attention_layer(aspect_outputs, length=self.n_hidden, name='aspect_att')
            _, self.context_att_reps = self._interactive_attention(batch_size=batch_size, weights=weights['context_score'],
                                                                    biases=biases['context_score'],
                                                                    max_len=self.max_context_len,
                                                                    aspect=aspect_att, context=context_outputs)
            ###############################################

            # self.reps = tf.concat([self.context_avg_reps, self.context_att_reps], 1)
            self.reps = tf.multiply(self.context_avg_reps, self.context_att_reps)

            gated_shared_variable = tf.get_variable(name='W_gated_shared',
                                                    shape=[self.n_hidden, self.n_hidden],
                                                    initializer=tf.random_uniform_initializer(-0.01, 0.01),
                                                    regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg)
                                                    )
            gated_shared_bias = tf.get_variable(name='B_shared',
                                                shape=[self.n_hidden, 1],
                                                initializer=tf.zeros_initializer(),
                                                regularizer=tf.contrib.layers.l2_regularizer(self.l2_reg))

            doc_output = self._gated_interation_layer(batch_size, gated_shared_variable, gated_shared_bias, doc_att, self.reps)
            context_output = self._gated_interation_layer(batch_size, gated_shared_variable, gated_shared_bias, self.reps, doc_att)

            self.scores = tf.matmul(context_output, weights['softmax']) + biases['softmax']
            self.doc_scores = tf.matmul(doc_output, weights['doc_softmax']) + biases['softmax']

        with tf.name_scope('loss'):
            self.aspect_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.scores, labels=self.labels))
            self.doc_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.doc_scores, labels=self.doc_labels))
            self.loss = self.aspect_loss + self.gamma * self.doc_loss

            self.global_step = tf.Variable(0, name="tr_global_step", trainable=False)
            # self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate, epsilon=1e-06).minimize(self.loss, global_step=self.global_step)
            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss, global_step=self.global_step)
            # self.optimizer = tf.train.AdadeltaOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

        with tf.name_scope('accuracy'):
            self.predicted = tf.argmax(self.scores, 1)
            self.actual = tf.argmax(self.labels, 1)
            self.correct_predictions = tf.equal(self.predicted, tf.argmax(self.labels, 1))
            self.accuracy = tf.reduce_sum(tf.cast(self.correct_predictions, tf.int32))

        with tf.name_scope('tracking'):
            # Keep track of gradient values (optional)
            summary_loss = tf.summary.scalar('loss', self.loss)
            summary_acc = tf.summary.scalar('acc', self.accuracy)

            self.train_summary_op = tf.summary.merge([summary_loss, summary_acc])
            self.test_summary_op = tf.summary.merge([summary_loss, summary_acc])

            timestamp = str(int(time.time()))
            self.out_dir = 'logs/' + str(timestamp) + '_r' + str(self.learning_rate) + '_b' + str(
                self.batch_size) + '_l' + str(
                self.l2_reg) + '_MULT_SHAREDE_SHAREDG' + '_' + str(self.domain)
            self.train_summary_writer = tf.summary.FileWriter(self.out_dir + '/train', self.sess.graph)
            self.test_summary_writer = tf.summary.FileWriter(self.out_dir + '/test', self.sess.graph)

            # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
            self.checkpoint_dir = os.path.abspath(os.path.join(self.out_dir, "checkpoints"))
            self.checkpoint_prefix = os.path.join(self.checkpoint_dir, "model")
            if not os.path.exists(self.checkpoint_dir):
                os.makedirs(self.checkpoint_dir)
            self.saver = tf.train.Saver(tf.global_variables())

            if self.checkpoint_path != '':
                self.load_model()


    def train_step(self, data):
        aspects, contexts, labels, aspect_lens, context_lens, aspect_lex, context_lex, doc_data, doc_labels, doc_lex = zip(*data)
        cost, cnt = 0.0, 0

        for sample, num in self.get_batch_data(aspects, contexts, labels, aspect_lens, context_lens, aspect_lex,
                                               context_lex, doc_data, doc_labels, doc_lex, self.batch_size, True, self.dropout):
            _, loss, step, summary = self.sess.run([self.optimizer, self.loss, self.global_step, self.train_summary_op],
                                                   feed_dict=sample)
            self.train_summary_writer.add_summary(summary, step)
            cost += loss * num
            cnt += num

        _, train_acc, precision, recall, f1 = self.eval_step(data)

        return float(cost) / cnt, train_acc

    def eval_step(self, data):
        aspects, contexts, labels, aspect_lens, context_lens, aspect_lex, context_lex, doc_data, doc_labels, doc_lex = zip(*data)
        cost, acc, cnt = 0.0, 0, 0
        precision, recall, f1 = 0., 0., 0.

        for sample, num in self.get_batch_data(aspects, contexts, labels, aspect_lens, context_lens, aspect_lex,
                                               context_lex, doc_data, doc_labels, doc_lex, len(data), False,
                                               1.0):
            loss, accuracy, step, summary, predicted, actual = self.sess.run(
            [self.loss, self.accuracy, self.global_step, self.test_summary_op, self.predicted, self.actual], feed_dict=sample)
            cost += loss * num
            acc += accuracy
            cnt += num
            self.test_summary_writer.add_summary(summary, step)
            precision, recall, f1 = self.evaluate_f_score(predicted, actual)

        return float(cost) / cnt, float(acc) / cnt, precision, recall, f1

    def get_batch_data(self, aspects, contexts, labels, aspect_lens, context_lens, aspect_lex, context_lex,
                       docs, doc_labels, doc_lex, batch_size, is_shuffle, keep_prob):
        aspects = np.array(aspects)
        contexts = np.array(contexts)
        labels = np.array(labels)
        aspect_lens = np.array(aspect_lens)
        context_lens = np.array(context_lens)
        context_lex = np.array(context_lex)
        aspect_lex = np.array(aspect_lex)
        doc_lex = np.array(doc_lex)
        docs = np.array(docs)
        doc_labels = np.array(doc_labels)

        for index in get_batch_index(len(aspects), batch_size, is_shuffle):
            feed_dict = {
                self.aspects: aspects[index],
                self.contexts: contexts[index],
                self.docs: docs[index],
                self.doc_labels: doc_labels[index],
                self.labels: labels[index],
                self.aspect_lens: aspect_lens[index],
                self.context_lens: context_lens[index],
                self.context_lex_embedding: context_lex[index],
                self.aspect_lex_embedding: aspect_lex[index],
                self.doc_lex_embedding: doc_lex[index],
                self.dropout_keep_prob: keep_prob,
            }
            yield feed_dict, len(index)

    def evaluate(self, test_data):
        test_loss, test_acc, p, r, f1 = self.eval_step(test_data)
        print('Acc=%s; Precision=%s; Recall=%s; f1=%s' % (test_acc, p, r, f1))

    def load_model(self):
        message = "Loading checkpoint from {}".format(self.checkpoint_path)
        print(message)

        ckpt = tf.train.get_checkpoint_state(self.checkpoint_path)
        if ckpt and ckpt.model_checkpoint_path:
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            fname = os.path.join(self.checkpoint_path, ckpt_name)
            self.saver.restore(self.sess, fname)
            message = "Checkpoint successfully loaded from {}".format(fname)
            print(message)
            return True
        else:
            message = "Checkpoint could not be loaded from {}".format(self.checkpoint_path)
            print(message)
            return False


    def train(self, train_data, test_data):
        print('Training...')
        self.sess.run(tf.global_variables_initializer())

        max_acc, step = 0., -1
        precision, recall, f1_score = 0., 0., 0.

        for i in range(self.n_epoch):
            train_loss, train_acc = self.train_step(train_data)
            test_loss, test_acc, p, r, f1 = self.eval_step(test_data)
            if test_acc > max_acc:
                max_acc = test_acc
                step = i
                self.saver.save(self.sess, self.checkpoint_prefix)
                f1_score = f1
                precision = p
                recall = r

            print('epoch %s: train-loss=%.6f; train-acc=%.6f; train-loss=%.6f; test-acc=%.6f; best-acc=%.6f; '
                  'precision=%.6f; recall=%.6f; f1-score=%.6f' % (
                    str(i), train_loss, train_acc, test_loss, test_acc, max_acc, precision, recall, f1_score))

        print('The max accuracy of testing results is %s of step %s' % (max_acc, step))
        print('Precision=%s; Recall=%s; f1=%s' % (precision, recall, f1_score))