# MLAANets
## An End-to-End Multi-task Lexicon-Aware Attention Network
The implementation of the paper: "*A Multi-task Lexicon-Aware Attention Network for Aspect-level Sentiment Classification*"

### Word & Lexicon Embeddings

Creating *vec* folder and copying embeddings into this folder.

[Glove 42B](https://nlp.stanford.edu/projects/glove/) is used for word embeddings

[Preconstructed-Lexicon Embeddings](https://drive.google.com/open?id=1CB1dyhsRGMk0El9ileUgLk49jepHoPjY)

[Yelp2014 - Amazon Electronics](https://drive.google.com/open?id=1I6Y3uZsh1DBT73woM1TeH_zQkX2lMrUr)

### Pre-trained Models

[Pre-trained_Model](https://drive.google.com/open?id=1DN0-YWUVmpp1XYnKejZGawHzulohRAMr)

### Running Models

```
python MLAANet_sharedE_sharedG.py
```
